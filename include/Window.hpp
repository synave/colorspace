#ifndef WINDOW_H
#define WINDOW_H

#include <QWidget>
#include <QListWidget>
#include <QPushButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QFileDialog>
#include "Image.hpp"

class QListWidget;
class GLWidget;

class Window : public QWidget
{
  Q_OBJECT
  
  public:
  Window(char* nom_image, char* colorspace[], Image *tabim[], int nb_colorspace);

public slots:
  void clic_sauver();

signals:
  void sauver_image(const QString& str, int resolution);
  void passage_2D_3D();

private:
  GLWidget *glWidget;
  QListWidget *canal1;
  QListWidget *canal2;
  QListWidget *canal3;
  QPushButton *sauver;
  QPushButton *passage_2D_3Db;
};

#endif
