#ifndef IMAGE_H
#define IMAGE_H

#include <assert.h>
#include <iostream>
#include <fstream>
#include <cstring>
#include <stdlib.h>
#include "Pixel.hpp"

using namespace std;

class Image{

public :
  int taillex,tailley;
  Pixel *pix;
  
public :
  Image();
  Image(string chemin);
  Image(int tx, int ty, int *canal1, int *canal2, int *canal3);
  Image(Image im1, int num_canal1, Image im2, int num_canal2, Image im3, int num_canal3);
  //Image(const Image &im);
  ~Image();

  int* extraire_canal(int num_canal);
  void sauver_image_fichier_ppm(const char *chemin);
  void affiche();
};
  
#endif
