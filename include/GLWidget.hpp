#ifndef GLWIDGET_H
#define GLWIDGET_H

#include <QGLWidget>
#include <QListWidget>
#include "Image.hpp"

class GLWidget : public QGLWidget
{
  Q_OBJECT

public:
  GLWidget(Image *tabim[]=NULL, QWidget *parent = 0);
  ~GLWidget();
  
  QSize minimumSizeHint() const;
  QSize sizeHint() const;
			
public slots:
  void modif_canal1(int row);
  void modif_canal2(int row);
  void modif_canal3(int row);
  void sauver_image(const QString& str, int resolution);
  void passage_2D_3D();
  
protected:
  void initializeGL();
  void paintGL();
  void resizeGL(int width, int height);
  void mousePressEvent(QMouseEvent *event);
  void mouseReleaseEvent(QMouseEvent *event);
  void mouseMoveEvent(QMouseEvent *event);
  void wheelEvent(QWheelEvent * event);
  void recompose();
  
private :
  void maj_pt_vue();
  Image **im;
  Image *composition;
  int canal1_row;
  int canal2_row;
  int canal3_row;
  double zoom;
  int transX,transY,rotX,rotY;
  int lastposx,lastposy;
  bool translation,rotation;
  bool mode2D;
};

#endif
