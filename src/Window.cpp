#include <QtGui>
#include <iostream>
#include <cstring>

#include "GLWidget.hpp"
#include "Window.hpp"

Window::Window(char* nom_image, char* colorspace[], Image *tabim[], int nb_colorspace)
{
  glWidget = new GLWidget(tabim);
  glWidget->setFixedSize(tabim[0]->taillex,tabim[0]->tailley);

  canal1 = new QListWidget;
  canal2 = new QListWidget;
  canal3 = new QListWidget;

  connect(canal1, 
	  SIGNAL(currentRowChanged(int)), 
          glWidget, 
	  SLOT(modif_canal1(int)));
  connect(canal2, 
	  SIGNAL(currentRowChanged(int)), 
          glWidget, 
	  SLOT(modif_canal2(int)));
  connect(canal3, 
	  SIGNAL(currentRowChanged(int)), 
          glWidget, 
	  SLOT(modif_canal3(int)));
  
  for(int i=0;i<nb_colorspace;i++)
    {
      QString str(colorspace[i]);
      str.append("_c1");
      canal1->addItem(str);
      canal2->addItem(str);
      canal3->addItem(str);
      str = colorspace[i];
      str.append("_c2");
      canal1->addItem(str);
      canal2->addItem(str);
      canal3->addItem(str);
      str = colorspace[i];
      str.append("_c3");
      canal1->addItem(str);
      canal2->addItem(str);
      canal3->addItem(str);
    }
  canal1->setCurrentRow(0);
  canal2->setCurrentRow(1);
  canal3->setCurrentRow(2);
  canal1->adjustSize();
  canal2->adjustSize();
  canal3->adjustSize();

  sauver=new QPushButton("Sauver l'image");

  connect(sauver, 
	  SIGNAL(released()), 
          this, 
	  SLOT(clic_sauver()));
  
  passage_2D_3Db=new QPushButton("2D/3D");

  connect(passage_2D_3Db, 
	  SIGNAL(released()), 
	  glWidget, 
	  SLOT(passage_2D_3D()));

  QVBoxLayout *sousLayout = new QVBoxLayout;
  sousLayout->addWidget(sauver);
  sousLayout->addWidget(passage_2D_3Db);

  QHBoxLayout *mainLayout = new QHBoxLayout;
  mainLayout->addWidget(glWidget);
  mainLayout->addWidget(canal1);
  mainLayout->addWidget(canal2);
  mainLayout->addWidget(canal3);
  mainLayout->addLayout(sousLayout);
  setLayout(mainLayout);

  setWindowTitle(QString("Melange image : ").append(nom_image));

  connect(this, 
	  SIGNAL(sauver_image(const QString&, int)), 
          glWidget, 
	  SLOT(sauver_image(const QString&, int)));
}

void Window::clic_sauver()
{
  QString str = QFileDialog::getSaveFileName(this,
					     "Sauver l'image sous...",
					     "",
					     tr("Images (*.ppm)"));
  int max=glWidget->width();
  if(glWidget->height()>max)
    max = glWidget->height();
  if(!str.isEmpty())
    emit sauver_image(str,(1023/max)+1);
}
