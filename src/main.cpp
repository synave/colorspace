#include <QApplication>
#include <iostream>
#include <stdlib.h>
#include <cstring>
#include "Window.hpp"
#include "Image.hpp"

const char* colorspace[20]={"CMY","CMYK","GRAY","HSB","HSL","HWB","LAB","LOG","OHTA","REC601YCBCR","REC709YCBCR","RGB","SRGB","TRANSPARENT","XYZ","YCBCR","YCC","YIQ","YPBPR","YUV"};

void usage(char* nom_prog)
{
  cout << "usage : " << nom_prog <<" <fichier image> <colorspace1> <colorspace2> ..." << endl;
  cout << "Les valeurs possibles des parametres <colorspace> sont les suivantes :"<< endl;
  for(int i=0;i<20;i++)
    cout << colorspace[i] << endl;
}

using namespace std;

int main(int argc, char *argv[])
{
  if(argc<3)
    {
      usage(argv[0]);
      return EXIT_FAILURE;
    }
  /*On verifie que le fichier passé en parametre existe bien*/
  ifstream is_image(argv[1]);
  if(is_image.fail())
    {
      cout << "fichier " << argv[1] << " introuvable" << endl;
      return EXIT_FAILURE;
    }
  else
    is_image.close();

  /*TODO VERIFIER LES EPSACES DE COULEURS*/
  for(int i=0;i<argc-2;i++)
    {
      for(unsigned int j=0;j<strlen(argv[i+2]);j++)
	if(argv[i+2][j]<='z' && argv[i+2][j]>='a')
	  argv[i+2][j]=argv[i+2][j]-32;
      string c(argv[i+2]);
      bool trouve=false;
      for(int j=0;j<20;j++)
	if(!c.compare(colorspace[j]))
	  trouve=true;
      if(!trouve)
	{
	  cout << "l'espace de couleur " << argv[i+2] << " n'est pas autorise" << endl;
	  usage(argv[0]);
	  return EXIT_FAILURE;
	}
    }

  /*extraction de l'espace de couleur si nécessaire*/
  for(int i=0;i<argc-2;i++)
    {
      /*test ouverture pour savoir si le fichier existe deja*/
      //char* nom_fichier=my_strcat(my_strcat(argv[1],"."),argv[i+2]);
      string nom_fichier(string(argv[1]).append(".").append(argv[i+2]));
      ifstream is_colorspace(nom_fichier.c_str());
      if(is_colorspace.fail())
	{
	  string command(string("./extract_colorspace ").append(argv[1]).append(" ").append(argv[i+2]));
	  system(command.c_str());
	  
	}
      else
	is_colorspace.close();
    }

  Image *tabim[argc-2];
  string chemin(argv[1]);
  chemin.append(".");
  for(int i=2;i<argc;i++)
    {
      string chemincomplet(chemin);
      chemincomplet.append(argv[i]);
      cout << "Ouverture du fichier " << chemincomplet << endl;
      tabim[i-2]=new Image(chemincomplet);
    }

  QApplication app(argc, argv);
  Window window(argv[1],argv+2,tabim,argc-2);
  window.show();
  return app.exec();
}
