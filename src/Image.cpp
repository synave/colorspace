#include "Image.hpp"

Image::Image()
{
  taillex=tailley=0;
  pix=NULL;
}

Image::Image(string chemin)
{
  ifstream is(chemin.c_str());
  int ret;
  string mn;

  assert(is.is_open());

  is >> mn;
  assert(!mn.compare("P3"));

  is >> taillex;
  is >> tailley;

  pix=new Pixel[taillex*tailley];
  assert(pix != NULL);

  is >> ret;

  for(int i=0;i<taillex*tailley;i++)
    {
      is >> pix[i][0];
      is >> pix[i][1];
      is >> pix[i][2];
    }

  is.close();
}

Image::Image(int tx, int ty, int *canal1, int *canal2, int *canal3)
{  
  taillex=tx;
  tailley=ty;

  pix=new Pixel[tx*ty];
  assert(pix != NULL);

  for(int i=0;i<tx*ty;i++)
    {
      pix[i][0]=canal1[i];
      pix[i][1]=canal2[i];
      pix[i][2]=canal3[i];
    }
}


Image::Image(Image im1, int num_canal1, Image im2, int num_canal2, Image im3, int num_canal3)
{
  assert(im1.taillex==im2.taillex && im1.taillex==im3.taillex &&
	 im1.tailley==im2.tailley && im1.tailley==im3.tailley &&
	 im2.taillex==im3.taillex && im2.tailley==im3.tailley);
  
  int *can1=im1.extraire_canal(num_canal1);
  int *can2=im2.extraire_canal(num_canal2);
  int *can3=im3.extraire_canal(num_canal3);
  
  taillex=im1.taillex;
  tailley=im1.tailley;
  
  pix=new Pixel[taillex*tailley];
  assert(pix != NULL);
  
  for(int i=0;i<taillex*tailley;i++)
    {
      pix[i][0]=can1[i];
      pix[i][1]=can2[i];
      pix[i][2]=can3[i];
    }

  delete[] can1;
  delete[] can2;
  delete[] can3;
}

int* Image::extraire_canal(int num_canal)
{
  assert(0<=num_canal && num_canal<=2);
  int *canal=new int[taillex*tailley];
  assert(canal != NULL);
  
  for(int i=0;i<taillex*tailley;i++)
    canal[i]=pix[i][num_canal];
  
  return canal;
}


void Image::sauver_image_fichier_ppm(const char *chemin)
{
  ofstream os(chemin);
  
  assert(os.is_open());
  
  os << "P3" << endl << taillex << " " << tailley << endl<< "255" << endl;
  for(int j=0;j<tailley;j++)
    {
      for(int i=0;i<taillex;i++){
	os << pix[j*taillex+i][0] << " " << pix[j*taillex+i][1] << " " << pix[j*taillex+i][2] << " ";
	os << endl;}
    }
  
  os.close();
}


Image::~Image()
{
  delete[] pix;
}

void Image::affiche()
{
  cout << "Taille : " << taillex << "x" << tailley << " pixels" << endl;
  for(int j=0;j<tailley;j++)
    {
      for (int i=0; i<taillex; i++)
	cout << "(" << pix[j*taillex+i][0] << "," << pix[j*taillex+i][1] << "," << pix[j*taillex+i][2] << ") ";
      cout << endl;
    }
}

