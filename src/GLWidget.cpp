#include <QtGui>
#include <QtOpenGL>
#include <cstring>

#include <math.h>
#include <iostream>

#include "GLWidget.hpp"

GLWidget::GLWidget(Image *tabim[], QWidget *parent)
  : QGLWidget(parent)
{
  im=tabim;
  canal1_row=0;
  canal2_row=0;
  canal3_row=0;
  zoom=1;
  transX=0;
  transY=0;
  rotX=0;
  rotY=0;
  translation=false;
  rotation=false;
  mode2D=true;
  recompose();
}

GLWidget::~GLWidget()
{
} 

void GLWidget::maj_pt_vue()
{
  if(mode2D)
    glViewport (transX, transY, composition->taillex*zoom, composition->tailley*zoom); 
  else
    glViewport (0, 0, composition->taillex, composition->tailley);
  glMatrixMode (GL_PROJECTION);
  glLoadIdentity ();
  if(mode2D)
    glOrtho(0,composition->taillex,0,composition->tailley,-10,10);
  else
    {
      if(composition->taillex==composition->tailley)
	glOrtho(-125,375,-125,375,-40000,40000);
      else
	{
	  int min,max;
	  if(composition->taillex>composition->tailley)
	    {
	      min=-125-(500-500*((composition->taillex*1.0)/(composition->tailley*1.0))/2);
	      max=375+(500-500*((composition->taillex*1.0)/(composition->tailley*1.0))/2);
	      glOrtho(min,max,-125,375,-40000,40000);
	    }
	  else
	    {
	      min=-125-(500-500*((composition->tailley*1.0)/(composition->taillex*1.0))/2);
	      max=375+(500-500*((composition->tailley*1.0)/(composition->taillex*1.0))/2);
	      glOrtho(-125,375,min,max,-40000,40000);
	    }
	}
    }
  glMatrixMode(GL_MODELVIEW);
}

QSize GLWidget::minimumSizeHint() const
{
  return QSize(im[0]->taillex, im[0]->tailley);
}

QSize GLWidget::sizeHint() const
{
  return QSize(im[0]->taillex, im[0]->tailley);
}

void GLWidget::modif_canal1(int row)
{
  canal1_row=row;
  recompose();
  updateGL();
}

void GLWidget::modif_canal2(int row)
{
  canal2_row=row;
  recompose();
  updateGL();
}

void GLWidget::modif_canal3(int row)
{
  canal3_row=row;
  recompose();
  updateGL();
}

void GLWidget::sauver_image(const QString& str, int resolution)
{
  string nom_fichier(QString(str).toStdString().data());
  if(nom_fichier.size()<4)
    nom_fichier.append(".png");
  if(nom_fichier[nom_fichier.size()-4]!='.')
    nom_fichier.append(".png");
  string extension=nom_fichier.substr(nom_fichier.size()-3,3);
  for(int i=0;i<3;i++)
    if(extension[i]<='z' && extension[i]>='a')
      extension[i]=extension[i]-32;
  if(string(extension).compare("PNG"))
    nom_fichier.append(".png");

  if(mode2D)
    resolution=1;

  QImage qim[resolution][resolution];
  for(int i=0;i<resolution;i++)
    for(int j=0;j<resolution;j++)
      {
	glViewport(-im[0]->taillex*i, -im[0]->tailley*j, im[0]->taillex*resolution, im[0]->tailley*resolution);
	updateGL();
	qim[i][j] = grabFrameBuffer();
      }
  
  QImage tosave(im[0]->taillex*resolution,im[0]->tailley*resolution,qim[0][0].format());
  
  for(int i=0;i<im[0]->taillex*resolution;i++)
    for(int j=0;j<im[0]->tailley*resolution;j++)
      tosave.setPixel(i, j, qim[i/im[0]->taillex][resolution-j/im[0]->tailley-1].pixel(i-((i/im[0]->taillex)*im[0]->taillex),j-((j/im[0]->tailley)*im[0]->tailley)));
  
  
  tosave.save(QString(nom_fichier.c_str()),"PNG");
  
  glViewport(0, 0, im[0]->taillex, im[0]->tailley);
  updateGL();
}

void GLWidget::passage_2D_3D()
{
  mode2D=!mode2D;
  transX=0;
  transY=0;
  zoom=1;
  maj_pt_vue();
  
  updateGL();
}

void GLWidget::initializeGL()
{
  qglClearColor(QColor::fromRgb(255,255,255));
  glClearDepth(1.0f);
  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_LEQUAL);
  glDisable(GL_CULL_FACE);
}

void GLWidget::paintGL()
{ 
  glClearColor(255.0,255.0,255.0,0.0);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glLoadIdentity();
  if(!mode2D)
    {
      glScalef(zoom,zoom,zoom);

      glTranslatef(transX,transY,0);

      glTranslatef(128,128,128);

      glRotatef(rotY/2.0,1.0,0.0,0.0);
      glRotatef(rotX/2.0,0.0,1.0,0.0);

      glTranslatef(-128,-128,-128);
    }
  else
    glPointSize(zoom);

  if(mode2D)
    {
      for(int j=0;j<composition->tailley;j++)
	for(int i=0;i<composition->taillex;i++)
	  {
	    glColor4f(((composition->pix[j*composition->taillex+i][0])*1.0)/255.0,
		      ((composition->pix[j*composition->taillex+i][1])*1.0)/255.0,
		      ((composition->pix[j*composition->taillex+i][2])*1.0)/255.0,1.0);
	    glBegin(GL_POINTS);
	    glVertex2i(i,composition->tailley-j);
	    glEnd();
	  }
    }
  else
    {
      /*Affichage du repère*/
	  glLineWidth(2.0);
	  glColor4f(1.0,0.0,0.0,1.0);
	  glBegin(GL_LINES);
	  glVertex3f(0,0,0);
	  glVertex3f(255,0,0);
	  glEnd();
	  glColor4f(0.0,1.0,0.0,1.0);
	  glBegin(GL_LINES);
	  glVertex3f(0,0,0);
	  glVertex3f(0,255,0);
	  glEnd();	
	  glColor4f(0.0,0.0,1.0,1.0);
	  glBegin(GL_LINES);
	  glVertex3f(0,0,0);
	  glVertex3f(0,0,255);
	  glEnd();
	  glPointSize (2.0);

	  for(int j=0;j<composition->tailley;j++)
	    for(int i=0;i<composition->taillex;i++)
	      {
		glColor4f(((composition->pix[j*composition->taillex+i][0])*1.0)/255.0,
			  ((composition->pix[j*composition->taillex+i][1])*1.0)/255.0,
			  ((composition->pix[j*composition->taillex+i][2])*1.0)/255.0,1.0);
		glBegin(GL_POINTS);
		glVertex3f(((composition->pix[j*composition->taillex+i][0])*1.0),
			   ((composition->pix[j*composition->taillex+i][1])*1.0),
			   ((composition->pix[j*composition->taillex+i][2])*1.0));
		glEnd();
	      }
    }
  
}

void GLWidget::resizeGL(int width, int height)
{
  width=height;
  height=width;
  maj_pt_vue();
}

void GLWidget::mousePressEvent(QMouseEvent *event)
{
  if (event->button() & Qt::RightButton)
    {
	  lastposx = event->x();
	  lastposy = event->y();
	  translation=true;
    }
  if (event->button() & Qt::MidButton)
    {
      transX=0;
      transY=0;
      rotX=0;
      rotY=0;
      zoom=1;
      maj_pt_vue();

      updateGL();
    }
  if(event->button() && Qt::LeftButton && !mode2D)
    {
       lastposx = event->x();
       lastposy = event->y();
       rotation=true;
    }
}

void GLWidget::mouseReleaseEvent(QMouseEvent *event)
{
  translation=false;
  rotation=false;
}

void GLWidget::mouseMoveEvent(QMouseEvent *event)
{
  if(translation)
    {
      double dx = 0, dy = 0;
      
      dx = event->x() - lastposx;
      dy = event->y() - lastposy;
      
      lastposx = event->x(); 
      lastposy = event->y(); 
      
      transX += dx;
      transY -= dy;
      
      maj_pt_vue();

      updateGL();
    }
  if(rotation)
    {
      double dx = 0, dy = 0;
      
      dx = event->x() - lastposx;
      dy = event->y() - lastposy;
      
      lastposx = event->x(); 
      lastposy = event->y(); 
      
      rotX += dx;
      rotY += dy;
      
      maj_pt_vue();

      updateGL();
    }
}

void GLWidget::wheelEvent(QWheelEvent * event)
{
  if(event->delta()>0)
    {
      if(zoom<15)
	{
	  if(mode2D)
	    zoom=zoom*2;
	  else
	    zoom*=1.1;
	}
    }
  else
    {
      if(zoom>1.0001)
	{
	  if(mode2D)
	    zoom=zoom/2;
	  else
	    zoom/=1.1;
	  if(transX>composition->taillex)
	    transX=0;
	  if(transY>composition->tailley)
	    transY=0;
	  if(-transX>composition->taillex*zoom)
	    transX=(-composition->taillex*zoom+composition->taillex);
	  if(-transY>composition->tailley*zoom)
	    transY=(-composition->tailley*zoom+composition->tailley);
	}
    }

  maj_pt_vue();
  
  updateGL();
}

void GLWidget::recompose()
{
  int mod1=canal1_row%3;
  int mod2=canal2_row%3;
  int mod3=canal3_row%3;
  int *can1=im[(canal1_row-mod1)/3]->extraire_canal(mod1);
  int *can2=im[(canal2_row-mod2)/3]->extraire_canal(mod2);
  int *can3=im[(canal3_row-mod3)/3]->extraire_canal(mod3);
  composition = new Image(im[0]->taillex,im[0]->tailley,can1,can2,can3);

  delete[] can1;
  delete[] can2;
  delete[] can3;
}
